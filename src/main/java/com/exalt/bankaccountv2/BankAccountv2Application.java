package com.exalt.bankaccountv2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BankAccountv2Application {

	public static void main(String[] args) {
		SpringApplication.run(BankAccountv2Application.class, args);
	}

}

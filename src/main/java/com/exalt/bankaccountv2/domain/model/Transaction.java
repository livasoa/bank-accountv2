package com.exalt.bankaccountv2.domain.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

public record Transaction(UUID transactionUuid, Account account, TransactionType transactionType, BigDecimal amount,
        Date transactionDate) {
}

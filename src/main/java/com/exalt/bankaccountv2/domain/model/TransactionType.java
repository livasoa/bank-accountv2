package com.exalt.bankaccountv2.domain.model;

import java.math.BigDecimal;

/**
 * Transaction should be a DEBIT ( - ) or a CREDIT ( + )
 */
public enum TransactionType {

    DEPOSIT {
        public BigDecimal computeBalance(BigDecimal balance, BigDecimal amount) {
            return null;
        }
    },
    WITHDRAW {
        public BigDecimal computeBalance(BigDecimal balance, BigDecimal amount) {
            return null;
        }
    };

    public abstract BigDecimal computeBalance(BigDecimal balance, BigDecimal amount);
}

package com.exalt.bankaccountv2.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.exalt.bankaccountv2.domain.model.Account;
import com.exalt.bankaccountv2.domain.model.TransactionType;
import com.exalt.bankaccountv2.domain.service.DepositMoneyService;
import com.exalt.bankaccountv2.domain.service.GetBalanceService;
import com.exalt.bankaccountv2.domain.service.GetTransactionsService;

public class DepositMoneyServiceTest {

    private DepositMoneyService service;
    private GetTransactionsService getTransactionsService;
    private GetBalanceService getBalanceService;

    Account account;

    @BeforeEach
    public void setup() {
        service = new DepositMoneyService();
        getTransactionsService = new GetTransactionsService();
        getBalanceService = new GetBalanceService();

        account = new Account(UUID.randomUUID(), new BigDecimal(250));

    }

    @ParameterizedTest(name = "should_add_new_credit_transaction_and_add_amount_to_balance")
    @CsvSource({ "1500, 1750", "750, 1000" })
    void should_add_new_credit_transaction_and_add_amount_to_balance(float amount, float expectedBalance) {
        service.depositMoney(account.getAccountUuid(), new BigDecimal(amount));
        assertNotNull(account);
        // Check if transaction added
        long nbDeposit = getTransactionsService.getTransactions(account.getAccountUuid()).stream()
                .filter(transaction -> transaction.transactionType() == TransactionType.DEPOSIT).count();
        assertEquals(1, nbDeposit);
        // Check if balance recalculated
        assertEquals(new BigDecimal(expectedBalance), getBalanceService.getBalance(account.getAccountUuid()));
    }

    // TODO: invalid amount

    // TODO: invalid accountUuid

    // TODO: Many transactions

}

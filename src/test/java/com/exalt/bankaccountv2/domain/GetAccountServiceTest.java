package com.exalt.bankaccountv2.domain;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.exalt.bankaccountv2.domain.model.Account;
import com.exalt.bankaccountv2.domain.service.GetAccountService;

public class GetAccountServiceTest {

    private GetAccountService service;

    UUID accountId = UUID.randomUUID();

    @BeforeEach
    public void setup() {
        service = new GetAccountService();
    }

    @Test
    void should_get_account() {
        Account account = service.getAccount(accountId);
        assertNotNull(account);
    }

}

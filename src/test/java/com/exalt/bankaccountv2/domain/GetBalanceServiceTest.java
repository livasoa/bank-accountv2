package com.exalt.bankaccountv2.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.exalt.bankaccountv2.domain.service.GetBalanceService;

public class GetBalanceServiceTest {

    private GetBalanceService service;

    UUID accountId = UUID.randomUUID();

    @BeforeEach
    public void setup() {
        service = new GetBalanceService();
    }

    @Test
    void should_get_balance_with_right_format() {
        BigDecimal balance = service.getBalance(accountId);
        assertNotNull(balance);
        assertEquals(2, balance.scale());
    }
}

package com.exalt.bankaccountv2.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.exalt.bankaccountv2.domain.model.Account;
import com.exalt.bankaccountv2.domain.model.Transaction;
import com.exalt.bankaccountv2.domain.model.TransactionType;
import com.exalt.bankaccountv2.domain.service.GetTransactionsService;

public class GetTransactionsServiceTest {

    private GetTransactionsService service;

    @BeforeEach
    public void setup() {
        service = new GetTransactionsService();
    }

    @Test
    void should_get_account_transaction_list() {
        UUID accountId = UUID.randomUUID();
        Account account = new Account(accountId, null);
        new Transaction(UUID.randomUUID(), account, TransactionType.DEPOSIT, new BigDecimal("250"),
                new Date());
        new Transaction(UUID.randomUUID(), account, TransactionType.DEPOSIT, new BigDecimal("420"),
                new Date());

        List<Transaction> transactions = service.getTransactions(accountId);
        assertNotNull(transactions);
        assertEquals(2, transactions.size());
    }

    @Test
    void should_return_empty_list() {
        UUID accountId = UUID.randomUUID();

        List<Transaction> transactions = service.getTransactions(accountId);
        assertNotNull(transactions);
        assertEquals(0, transactions.size());
    }

}

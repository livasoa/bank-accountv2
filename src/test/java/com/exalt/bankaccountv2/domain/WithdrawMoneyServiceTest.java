package com.exalt.bankaccountv2.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import com.exalt.bankaccountv2.domain.model.Account;
import com.exalt.bankaccountv2.domain.model.TransactionType;
import com.exalt.bankaccountv2.domain.service.GetBalanceService;
import com.exalt.bankaccountv2.domain.service.GetTransactionsService;
import com.exalt.bankaccountv2.domain.service.WithdrawMoneyService;

public class WithdrawMoneyServiceTest {
    private WithdrawMoneyService service;
    private GetTransactionsService getTransactionsService;
    private GetBalanceService getBalanceService;

    Account account;

    @BeforeEach
    public void setup() {
        service = new WithdrawMoneyService();
        getTransactionsService = new GetTransactionsService();
        getBalanceService = new GetBalanceService();

        account = new Account(UUID.randomUUID(), new BigDecimal(250));

    }

    @ParameterizedTest(name = "should_add_new_credit_transaction_and_add_amount_to_balance")
    @CsvSource({ "100, 150", "750, -500" })
    void should_add_new_debit_transaction_and_add_amount_to_balance(float amount, float expectedBalance) {
        service.withdrawMoney(account.getAccountUuid(), new BigDecimal(amount));
        assertNotNull(account);
        // Check if transaction added
        long nbWithdraw = getTransactionsService.getTransactions(account.getAccountUuid()).stream()
                .filter(transaction -> transaction.transactionType() == TransactionType.WITHDRAW).count();
        assertEquals(1, nbWithdraw);
        // Check if balance recalculated
        assertEquals(new BigDecimal(expectedBalance), getBalanceService.getBalance(account.getAccountUuid()));
    }

    // TODO: invalid amount

    // TODO: invalid accountUuid

    // TODO: Many transactions
}
